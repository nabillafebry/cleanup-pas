package id.co.asyst.amala.cleanup.pas.utils;

import org.apache.camel.Exchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {
    private static Logger log = LogManager.getLogger(Utils.class);

    public void getTodayDate(Exchange exchange) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.HOUR_OF_DAY, 7);

        exchange.setProperty("todaydate", formatter.format(c.getTime()));
    }

    public void setCounter(Exchange exchange) {
        int counter = 0;

        exchange.setProperty("counter", counter);
    }

    public void checkSync(Exchange exchange) {
        System.gc();
        List<Map<String, Object>> listSyncMember = (List<Map<String, Object>>) exchange.getProperty("listsyncmember");
        List<String> listNotDelete = new ArrayList<>();
        Boolean delete = true;

        int reject = 0;
        int accept = 0;
        int blue = 0;
        int citi = 0;
        int gold = 0;
        int plat = 0;
        int slvr = 0;
        int torb = 0;

        StringBuilder query = new StringBuilder();

        query.append("delete from amala_db.pas_sync " +
                "where memberid = '"+exchange.getProperty("memberid").toString()+"' "+
                "and operation = 'UPDATE' and syncid not in (");


        for (Map<String, Object> sync : listSyncMember) {
            if (sync.get("status").toString().equals("ACCEPTED")) {
                accept++;
                if (accept == 1) {
                    sync.put("isfirst", true);
                } else {
                    sync.put("isfirst", false);
                }
            } else {
                reject++;
                if (reject == 1) {
                    sync.put("isfirst", true);
                } else {
                    sync.put("isfirst", false);
                }
            }
        }


        for (Map<String, Object> sync : listSyncMember) {
            if (sync.get("isfirst").equals(true)) {
                if (sync.get("request") != null) {
                    if (sync.get("request").toString().contains("BLUE")) {
                        blue++;
                        query.append("'"+sync.get("syncid").toString()+"',");
                    }
                    if (sync.get("request").toString().contains("CITI")) {
                        citi++;
                        query.append("'"+sync.get("syncid").toString()+"',");
                    }
                    if (sync.get("request").toString().contains("GOLD")) {
                        gold++;
                        query.append("'"+sync.get("syncid").toString()+"',");
                    }
                    if (sync.get("request").toString().contains("PLAT")) {
                        plat++;
                        query.append("'"+sync.get("syncid").toString()+"',");
                    }
                    if (sync.get("request").toString().contains("SLVR")) {
                        slvr++;
                        query.append("'"+sync.get("syncid").toString()+"',");
                    }
                    if (sync.get("request").toString().contains("TORB")) {
                        torb++;
                        query.append("'"+sync.get("syncid").toString()+"',");
                    }
                }
            } else {
                if (sync.get("request") != null) {
                    if (sync.get("request").toString().contains("BLUE")) {
                        if (blue > 0) {
//                            listDelete.add(sync.get("syncid").toString());
                        } else {
                            blue++;
                            query.append("'"+sync.get("syncid").toString()+"',");
                        }
                    }
                    if (sync.get("request").toString().contains("CITI")) {
                        if (citi > 0) {
//                            listDelete.add(sync.get("syncid").toString());
                        } else {
                            citi++;
                            query.append("'"+sync.get("syncid").toString()+"',");
                        }
                    }
                    if (sync.get("request").toString().contains("GOLD")) {
                        if (gold > 0) {
//                            listDelete.add(sync.get("syncid").toString());
                        } else {
                            gold++;
                            query.append("'"+sync.get("syncid").toString()+"',");
                        }
                    }
                    if (sync.get("request").toString().contains("PLAT")) {
                        if (plat > 0) {
//                            listDelete.add(sync.get("syncid").toString());
                        } else {
                            plat++;
                            query.append("'"+sync.get("syncid").toString()+"',");
                        }
                    }
                    if (sync.get("request").toString().contains("SLVR")) {
                        if (slvr > 0) {
//                            listDelete.add(sync.get("syncid").toString());
                        } else {
                            slvr++;
                            query.append("'"+sync.get("syncid").toString()+"',");
                        }
                    }
                    if (sync.get("request").toString().contains("TORB")) {
                        if (torb > 0) {
//                            listDelete.add(sync.get("syncid").toString());
                        } else {
                            torb++;
                            query.append("'"+sync.get("syncid").toString()+"',");
                        }
                    }
                } else {
//                    listDelete.add(sync.get("syncid").toString());
                }
            }
        }

        if (query.toString().contains(",")){
            exchange.setProperty("delete", delete);
            query.deleteCharAt(query.length() - 1);
//            query.append(String.join(",",listNotDelete));
            query.append(")");
            log.info("QUERY::"+query);
            exchange.setProperty("query", query);
        }else {
            delete = false;
            exchange.setProperty("delete", delete);
        }
    }
}
