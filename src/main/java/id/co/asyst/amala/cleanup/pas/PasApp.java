package id.co.asyst.amala.cleanup.pas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"cleanup-pas.xml", "beans.xml"})
public class PasApp {

    public static void main(String[] args) {
        SpringApplication.run(PasApp.class, args);
    }
}