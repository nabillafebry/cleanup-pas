package id.co.asyst.amala.cleanup.pas.utils;

public class HandlingValidateException extends Exception {

    public HandlingValidateException(String message) {
        super(message);
    }
}
