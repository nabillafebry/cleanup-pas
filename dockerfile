# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY /cleanup-pas-1.2.0-SNAPSHOT.jar /cleanup-pas-1.2.0-SNAPSHOT.jar
# run application with this command line[
CMD ["java", "-jar", "/cleanup-pas-1.2.0-SNAPSHOT.jar"]
